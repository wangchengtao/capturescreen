package com.dylan_wang.capturescreen;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.black.copyfloatingwindow.AppConnect;

public class Newpage extends Activity implements OnClickListener  {
    TextView showonlinetext2, showonlinetext, mymoneyTV;
    String tag = "这里是悬浮时间newpage页面";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newpage);


        // 获取各种文本对象
        showonlinetext = (TextView) findViewById(R.id.showonlinetext);// 上边提示文本
        showonlinetext2 = (TextView) findViewById(R.id.showonlinetext2);// 下边的提示文本
        mymoneyTV = (TextView) findViewById(R.id.mymoneyTV);// 获取显示积分余额 文本view



        // 万普在线参数 设置提示view
        showonlinetext.setText(AppConnect.getInstance(this).getConfig("showonlinetext",
                "1.建议您使用WiFi下载软件(首次使用建议您点击申请填写个人资料)\n2.下载试玩后可获得相应积分,您可以点击申请兑换Q币话费支付宝红包的等超值礼品哦!\n3.加入官方群就送Q币哦\n4.请按照相应任务提示,积分一般五到十分钟到账\n5.如果有积分未到账请及时联系客服\n6.悬浮时间保证所有活动的真实!"));
        // 下边提示文本
        if (!AppConnect.getInstance(this).getConfig("showonlinetext2", "1").equals("1")) {
            showonlinetext2.setText(AppConnect.getInstance(this).getConfig("showonlinetext2", "1"));
        }
        /** test *************************************************************************************/
        String android_id = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);

//        TelephonyManager tm = (TelephonyManager) this.getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
//        String imei = tm.getDeviceId();

//        Log.d(tag, "这个安卓的id是" + android_id + "\nIMEI的唯一值是:" + imei);

    }

    // 展示积分墙
    public void bn1(View v) {

    }

    // 打开申请页面
    public void openpage(View v) {
        Log.d(tag, "点击申请页面按钮");
        // 跳转页面


    }

    // 打开开发者留言界面
    public void decmsg(View v) {
        Log.d(tag, "点击开发者留言按钮");
        // 调用反馈建议接口
        Toast.makeText(this, "我们会在24小时之内回复你哦!", Toast.LENGTH_SHORT).show();
        AppConnect.getInstance(this).showFeedback(this);
    }

    // 复制群号到剪贴板
    public void copyteamnum(View v) {
        // 把群号复制到手机剪贴板

        if (joinQQGroup("2rt4XXB58RwW3o8CIn7oFBmH4WtCE0ou")) {

        } else {

            ClipboardManager clipboardManager2 = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            clipboardManager2.setPrimaryClip(ClipData.newPlainText(null, "374344451"));
            if (clipboardManager2.hasPrimaryClip()) {
                clipboardManager2.getPrimaryClip().getItemAt(0).getText();
            }

            Toast.makeText(this, " QQ群号已复制!\n^_^多多在群里互动哟", Toast.LENGTH_SHORT).show();
        }

    }

    /****************
     *
     * 发起添加群流程。群号：悬浮时间用户讨论群(374344451) 的 key 为： 2rt4XXB58RwW3o8CIn7oFBmH4WtCE0ou
     * 调用 joinQQGroup(2rt4XXB58RwW3o8CIn7oFBmH4WtCE0ou) 即可发起手Q客户端申请加群
     * 悬浮时间用户讨论群(374344451)
     *
     * @param key
     *            由官网生成的key
     * @return 返回true表示呼起手Q成功，返回fals表示呼起失败
     ******************/
    public boolean joinQQGroup(String key) {
        Intent intent = new Intent();
        intent.setData(Uri
                .parse("mqqopensdkapi://bizAgent/qm/qr?url=http%3A%2F%2Fqm.qq.com%2Fcgi-bin%2Fqm%2Fqr%3Ffrom%3Dapp%26p%3Dandroid%26k%3D"
                        + key));
        // 此Flag可根据具体产品需要自定义，如设置，则在加群界面按返回，返回手Q主界面，不设置，按返回会返回到呼起产品界面
        // //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        try {
            startActivity(intent);
            return true;
        } catch (Exception e) {
            // 未安装手Q或安装的版本不支持
            return false;
        }
    }

    // 奖励积分
    public void bn2(View v) {
        // 该接口直接返回增加积分结果，成功返回 true，否则返回 false。


    }

    // 消费积分
    public void bn3(View v) {
        // 该接口直接返回增加积分结果，成功返回 true，否则返回 false。


    }

    // 查询积分
    public void bn4(View v) {

//        Toast.makeText(this, "现在的积分是:" + pointsBalance, Toast.LENGTH_SHORT).show();

    }

    /**
     * 积分订单赚取时会回调本方法（本回调方法执行在UI线程中）
     */
//    @Override
//    public void onPointEarn(Context arg0, EarnPointsOrderList list) {
//        // 遍历订单
//        for (int i = 0; i < list.size(); ++i) {
//            EarnPointsOrderInfo info = list.get(i);
//            Toast.makeText(this, info.getMessage(), Toast.LENGTH_LONG).show();
//        }
//    }

//    @Override
//    public void onPointBalanceChange(float pointsBalance) {
//        mymoneyTV.setText("您的积分有:" + (int) pointsBalance);
//    }

    @Override
    public void onClick(View arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
//        PointsManager.getInstance(this).unRegisterNotify(this);
//        // 有米积分墙的资源回收
//        OffersManager.getInstance(getApplicationContext()).onAppExit();
    }

}

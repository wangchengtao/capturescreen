package com.dylan_wang.capturescreen;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Toast;


import com.black.copyfloatingwindow.AppConnect;

public class StartLayout extends Activity {
    String tag = "StartLayout------>";
    private SharedPreferences        spf;
    private SharedPreferences.Editor spfEdi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        // 去标题栏
        Window window = this.getWindow();
        window.requestFeature(window.FEATURE_NO_TITLE);
        setContentView(R.layout.start_layout);
        Log.e(tag, "加载布局成功");

        /** 各种sdk初始化 ******************************************************************************************************/
        // 初始化万普：通过代码设置 APP_ID 和 APP_PID
        AppConnect.getInstance("d822480adb48e657e593be97bb243045", "QQ", this);


        // 初始化卸载广告
        // AppConnect.getInstance(this).initUninstallAd(this);
        AppConnect.getInstance(this).initUninstallAd(this);
        // 初始化分享广告
        // String wx_appid =
        // "bc7801a4875a144aa67695e6301cf3a8";//微信开放平台申请审核通过后可获得
        // AppConnect.getInstance(this).setWeixinAppId(wx_appid, this);
        // AppConnect.getInstance(this).showShareOffers(this);


        // 错误报告
        AppConnect.getInstance(this).setCrashReport(true);// 默认值true开吭，设置 false

        Log.d(tag, "各种SDK加载都没有问题!");
        /******************************************************************************************************************/

        //        // 获取本地存储对象
        //        spf = getSharedPreferences("startAct", MODE_WORLD_WRITEABLE);
        //        spfEdi = spf.edit();
        //        // 如果是第一次登陆则显示对话框
        //
        //        Log.e(tag, "获取本地存储对象没有问题!" + spf.contains("first"));
        //
        //        // if (spf.getBoolean("first", true)) {
        //        // dialo                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              g();
        //        // }
        //
        //        Log.e(tag, "本地存储没有问题!");

    }

    public void startButton(View v) {
        Log.e(tag, "启动按钮点击成功");
        // System.out.println("启动按钮点击成功");
        Toast.makeText(this, "如果没有显示,请进入设置打开APP悬浮窗权限~", Toast.LENGTH_LONG).show();
        // 启动显示秒的悬浮窗
        Intent intentSecond = new Intent(getApplicationContext(), FxService.class);
        if (intentSecond != null) {
            Log.e(tag, "如果显示秒的不等于null则关闭的方法已经执行!");
            stopService(intentSecond);
        }
        startService(intentSecond);// 注意是startService

        // finish();

    }

    public void RemoveButton(View v) {
        Log.e(tag, "移除按钮点击成功");
        System.out.println("移除按钮点击成功");
        // 移除悬浮窗
        Intent intentSecond = new Intent(getApplicationContext(), FxService.class);
        if (intentSecond != null) {
            Log.e(tag, "如果显示秒的不等于null则关闭的方法已经执行!");
            stopService(intentSecond);


            Intent inten1t = new Intent(getApplicationContext(), Service1.class);
            stopService(inten1t);
        }
    }

    public void setting(View v) {
        Log.e(tag, "设置按钮点击成功");
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }

    // 应用第一次打开时的对话框
    private void dialog() {
        new AlertDialog.Builder(getApplicationContext()).setTitle("写在前面的话").setIcon(R.mipmap.ic_launcher)
                // 设置标题
                .setMessage(
                        "更新应广大网友要求，临时加入了显示秒的悬浮窗，并且解决了双显示问题，不过请大家知悉，显示秒的悬浮窗会出现卡顿、停止等情况等情况。\n如果无法移除悬浮窗口请点击移除或者强制关闭。\n如有闪退等情况请及时反馈哦！\n点击不再显示可以复制群号码哦！")
                // 设置提示消息
                .setPositiveButton("不再显示", new DialogInterface.OnClickListener() {// 设置确定的按键
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // 写入本地数据库
                        spfEdi.putBoolean("first", false).commit();

                        // 把群号复制到手机剪贴板
                        ClipboardManager clipboardManager2 = (ClipboardManager) getSystemService(
                                Context.CLIPBOARD_SERVICE);
                        clipboardManager2.setPrimaryClip(ClipData.newPlainText(null, "374344451"));
                        if (clipboardManager2.hasPrimaryClip()) {
                            clipboardManager2.getPrimaryClip().getItemAt(0).getText();
                        }

                        Toast.makeText(getApplicationContext(), "QQ群号已复制到剪贴板！", Toast.LENGTH_LONG).show();

                    }
                }).setNegativeButton("取消", new DialogInterface.OnClickListener() {// 设置取消按键
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

            }
        }).setCancelable(false)// 设置按返回键是否响应返回，true是响应
                .show();// 显示

    }

    @Override
    protected void onDestroy() {
        // TODO 自动生成的方法存根
        super.onDestroy();
        // 调用万普的结束接口
        AppConnect.getInstance(this).close();
        // 有米积分墙的资源回收
        // OffersManager.getInstance(getApplicationContext()).onAppExit();
    }

    public void onResume() {
        super.onResume();

    }

    public void onPause() {
        super.onPause();

    }

}
